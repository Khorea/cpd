import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerClientsHandler extends Thread {

    private Socket receiver;
    private BufferedReader in;
    private PrintWriter out;

    ServerClientsHandler(Socket receiver)
    {
        this.receiver = receiver;
    }

    @Override
    public void run ()
    {
        try {
            in = new BufferedReader(new InputStreamReader(receiver.getInputStream()));
            out = new PrintWriter(receiver.getOutputStream(), true);

            String message = in.readLine();

            if (message != null && message.equals("ok")) {
                Publisher publisher = new Publisher();
                publisher.start();
                Thread.sleep(20000);
                publisher.interrupt();
                Client.sendToken("127.0.0.1", 5050);
            }

            in.close();
            out.close();
            receiver.close();
        } catch (IOException | InterruptedException ioException) {
            ioException.printStackTrace();
        }
    }
}