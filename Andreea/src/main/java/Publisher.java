import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.util.Scanner;

public class Publisher extends Thread {

    private static final String EXCHANGE_NAME = "News";

    public static void publish (int topic, String message) {

        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            channel.exchangeDeclare(EXCHANGE_NAME, "direct");

            if (topic == 1) { // Sport
                channel.basicPublish(EXCHANGE_NAME, "Sport", null, message.getBytes("UTF-8"));
                System.out.println(" [x] Sent '" + "Sport" + "':'" + message + "'");
            }
            else if (topic == 2) { // Harry Potter
                channel.basicPublish(EXCHANGE_NAME, "Harry_Potter", null, message.getBytes("UTF-8"));
                System.out.println(" [x] Sent '" + "Harry Potter" + "':'" + message + "'");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        boolean publishing = true;

        while (publishing) {
            Scanner sc = new Scanner(System.in);

            System.out.println("Publish on:");
            System.out.println("");
            System.out.println("1. Sport");
            System.out.println("2. Harry Potter");
            int topic = Integer.parseInt(sc.nextLine());
            System.out.println("Message: ");
            String message = sc.nextLine();

            Publisher.publish(topic, message);

            System.out.println("Publish something else? (y/n)");

            if (!sc.nextLine().equals("y")) {
                publishing = false;
            }
        }
    }
}
