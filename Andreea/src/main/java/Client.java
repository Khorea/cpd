import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {

    public static void sendToken(String IP, int PORT) throws IOException {
        String token = "ok";
        PrintWriter out;
        Socket sender = new Socket(IP, PORT);
        out = new PrintWriter(sender.getOutputStream(), true);
        out.println(token);

        out.close();
        sender.close();
    }
}
