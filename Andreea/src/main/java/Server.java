import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {

    private static int PORT;
    private static ServerSocket andreeaServer;

    Server(int PORT)
    {
        this.PORT = PORT;
    }

    @Override
    public void run ()
    {
        try {
            andreeaServer = new ServerSocket(PORT);

            while (true) {
                Socket receiver = andreeaServer.accept();
                new ServerClientsHandler(receiver).start();
            }
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
