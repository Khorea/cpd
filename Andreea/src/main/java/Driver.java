import java.io.IOException;

public class Driver {

    public static void main(String args[]) throws IOException {
        Server server = new Server(6060);
        server.start();

        Subscriber subscriber = new Subscriber();
        subscriber.start();
    }
}
