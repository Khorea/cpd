import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {

    private static int PORT;
    private static ServerSocket timoteiServer;

    Server(int PORT)
    {
        this.PORT = PORT;
    }

    @Override
    public void run ()
    {
        try {
            timoteiServer = new ServerSocket(PORT);

            while (true) {
                Socket receiver = timoteiServer.accept();
                new ServerClientsHandler(receiver).start();
            }
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
