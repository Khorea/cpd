import java.io.IOException;

public class Driver {

    public static void main(String args[]) throws IOException {
        Server server = new Server(5050);
        server.start();

        Client.sendToken("127.0.0.1", 6060);

        Subscriber subscriber = new Subscriber();
        subscriber.start();
    }
}
